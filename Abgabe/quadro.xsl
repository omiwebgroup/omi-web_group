<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <html>
      <head>
        <title>quadkopterpilot.de</title>
      </head>
      <body>
        <xsl:apply-templates />
        <br />
      </body>
    </html>
  </xsl:template>
  <xsl:template match="Überschrift">
    <h1>
      <xsl:value-of select="." />
    </h1>
  </xsl:template>
  <xsl:template match="Absatz">
    <p>
      <xsl:value-of select="." />
    </p>
  </xsl:template>
</xsl:stylesheet>
