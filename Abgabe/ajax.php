<?php
  echo "Ich erwachte zu einem s&uuml;&szlig;en Leben im Schoos duftiger B&uuml;sche; leise murmelte ein Bach durch blumige Wiesen, und der blaue Himmel schaute ruhig und klar durch das gr&uuml;ne Gezweig als ich mich zum ersten Mal umschaute in der Welt. &ndash;";
?>
