// Neues Objekt vom Typ DATE
var zeitObjekt = new Date();
// Variable mit aktueller STUNDE erzeugen
var uhrZeit = zeitObjekt.getHours();
// Wenn Stunden kleiner 12
if (uhrZeit < 12) {
    // Schreibe "Guten Morgen"
    document.write("Guten Morgen");
}
// oder wenn Stunden größer 12 und kleiner 17 schreibe "Guten Tag"
else if (uhrZeit > 12) {
    if (uhrZeit < 17) {
        document.write("Guten Tag");
    }
    // sonst schreibe "Guten Abend"
} else {
    document.write("Guten Abend");
}
