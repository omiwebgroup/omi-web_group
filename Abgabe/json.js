var xmlHttp = null;
// Wenn Browserfenster ActiveXObject
if (window.ActiveXObject) {
    // Extrawurst für IE8 und älter
    try {
        xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
    } catch (e) {
        try {
            xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
        } catch (e) {
            alert("Objekt nicht erfolgreich erstellt!");
        }
    }
    // XMLHttpRequest Objekt für andere Browser z.B.: Firefox
    // Sonst wenn Browserfenster XMLHttpRequest 
} else if (window.XMLHttpRequest) {
    xmlHttp = new XMLHttpRequest();
}
if (xmlHttp != null) {
    // Öffne/Hole json-links.txt
    xmlHttp.open("GET", "./json-links.txt", true);
    // Rufe Funktion ausgabe auf wenn sich readyState ändert
    xmlHttp.onreadystatechange = ausgabe;
    xmlHttp.send(null);
}
    function ausgabe() {
        if (xmlHttp.readyState == 4) {
            // HTTP Antwort als Text
            var daten = xmlHttp.responseText;
            // JSON Textdaten mit equal() in Javascript Array mit Objekten wandeln
            datenObjekt = eval('(' + daten + ')');
            // DOM Objekt mit id listing der Variablen liste zuweisen
            var liste = document.getElementById("listing");
            // solange bis alle Elemente in in HTML konvertiert sind
            for (var i = 0; i < datenObjekt.length; i++) {
                var link = datenObjekt[i];
                var li = document.createElement("li");
                var a = document.createElement("a");
                a.setAttribute("href", link.url);
                var txt = document.createTextNode(link.text);
                // Hänge an "Vaterknoten"
                a.appendChild(txt);
                li.appendChild(a);
                liste.appendChild(li);
            }
        }
    }
