var xmlHttpAjax = null;
// Wenn Browserfenster ActiveXObject
if (window.ActiveXObject) {
    // Extrawurst für IE8 und älter
    try {
        xmlHttpAjax = new ActiveXObject("Msxml2.XMLHTTP");
    } catch (e) {
        try {
            xmlHttpAjax = new ActiveXObject("Microsoft.XMLHTTP");
        } catch (e) {
            alert("Objekt nicht erfolgreich erstellt!");
        }
    }
    // XMLHttpRequest Objekt für andere Browser z.B.: Firefox
    // Sonst wenn Browserfenster XMLHttpRequest 
} else if (window.XMLHttpRequest) {
    xmlHttpAjax = new XMLHttpRequest();
}

function gedicht() {
	if(xmlHttpAjax != null){
		xmlHttpAjax.open("GET", "ajax.php", true);
		xmlHttpAjax.onreadystatechange = ausgabe;
		xmlHttpAjax.send(null);
	}
}


function ausgabe() {
    if (xmlHttpAjax.readyState == 4) {
        document.getElementById("gedicht").insertAdjacentHTML("AfterBegin", xmlHttpAjax.responseText);
    }
}
