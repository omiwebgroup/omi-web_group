/**
 * Created by asatru_h on 05.04.14.
 */

/* Array mit Bildersammlung */
var bildershow = new Array("JS/img/1.1.png", "JS/img/1.2.png", "JS/img/1.5.png", "JS/img/1.6.png", "JS/img/1.7.png", "JS/img/1.8.png", "JS/img/1.9.png", "JS/img/1.10.png");
var nextpic = 0;
var time = 3000;

function animation(){
    document.getElementById('bshow').src = bildershow[nextpic];
    nextpic++;

    if (nextpic == bildershow.length){
        nextpic = 0;
    }

    setTimeout("animation();", time);
}

