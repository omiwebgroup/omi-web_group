/**
 * Created by Marc Enders on 03.07.14.
 */
/*Funktion prüft ob entsprechende Felder ausgefüllt sind*/
function chkFormular () {
    if (document.Formular.vorname.value == "") {
        alert("Bitte Ihren Vornamen eingeben!");
        /*Fokus auf entsprechende Position setzen*/
        document.Formular.vorname.focus();
        return false;
    }
    if (document.Formular.nachname.value == "") {
        alert("Bitte Ihren Nachnamen eingeben!");
        document.Formular.nachname.focus();
        return false;
    }
    if (document.Formular.mail.value == "") {
        alert("Bitte Ihre E-Mail-Adresse eingeben!");
        document.Formular.mail.focus();
        return false;
    }
    /*indexOf gibt -1 zurück wenn Element nicht enthalten ist*/
    if (document.Formular.mail.value.indexOf("@") == -1) {
        alert("Keine E-Mail-Adresse!");
        document.Formular.mail.focus();
        return false;
    }
    if (document.Formular.mail.value.indexOf(".") == -1) {
        alert("Keine E-Mail-Adresse!");
        document.Formular.mail.focus();
        return false;
    }
}