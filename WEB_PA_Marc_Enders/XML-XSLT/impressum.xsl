<?xml version="1.0" encoding="ISO-8859-1" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <html xmlns="http://www.w3.org/1999/xhtml">
            <head>
                <title>Impressum</title>
                <link rel="stylesheet" type="text/css" href="../CSS/Style.css"/>
	         <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
            </head>
            <body>
                <div id="outer_container">
                    <div id="container">
                        <div id="top"></div>
                        <div id="logo"></div>
                        <div>
                            <ul id="menue_nav">
                                <li><a href="../index.html">Startseite</a></li>
                                <li><a href="../news.html">News</a></li>
                                <li><a href="../umbau.html">Umbau</a></li>
                                <li><a href="../links.html">Links</a></li>
                                <li><a href="../other.html">Other</a></li>
                                <li><a class="changecolor_after_click_green" href="../impressum.html">Impressum</a></li>
                            </ul>
                        </div>
                        <div id="inhalt_background_wallpaper">
                            <div id="greenbox_main" style="text-align: center">
                                <p class="h1_ueberschrift_left_15">
                                    Impressum: #XML #XSL #DTD
                                </p>
                                <p class="h4_ueberschrift_impressum_30">
                                    <xsl:value-of select="Impressum/�berschrift"/>
                                </p>
                                <p class="h2_black_text_16">
                                    <xsl:value-of select="Impressum/TMG-Auszug"/>
                                </p>
                                <table class="h2_black_text_13 tabelle_zentrieren tabelle_spacing tabelle_border">
                                    <tr>
                                        <td>
                                            Vorname:
                                        </td>
                                        <td>
                                            <xsl:value-of select="Impressum/Vorname"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Nachname:
                                        </td>
                                        <td>
                                            <xsl:value-of select="Impressum/Nachname"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Stra�e:
                                        </td>
                                        <td>
                                            <xsl:value-of select="Impressum/Stra�e"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Nr.:
                                        </td>
                                        <td>
                                            <xsl:value-of select="Impressum/Nr"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            PLZ:
                                        </td>
                                        <td>
                                            <xsl:value-of select="Impressum/PLZ"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Ort:
                                        </td>
                                        <td>
                                            <xsl:value-of select="Impressum/Ort"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Telefon:
                                        </td>
                                        <td>
                                            <xsl:value-of select="Impressum/Telefon"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            e-Mail:
                                        </td>
                                        <td>
                                            <xsl:value-of select="Impressum/Mail"/>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>
