/**
 * Created by Marc Enders on 03.07.14.
 */
var xmlHttp = null;

//Browserkompatiblität, Funktionalität auf allen Browsern gewährleisten
if (window.XMLHttpRequest) {
    //Objekt erzeugen
    xmlHttp = new XMLHttpRequest();
} else if (window.ActiveXObject) {
    //Objekt erzeugen
    xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
}

//An Webserver senden
function run(){
    if (xmlHttp != null) {
        //Webserver führt hinterlegte Datei aus
        xmlHttp.open("GET", "JSON/links.txt", true);
        //Callback-Funktion, Wird zurückgerufen wenn Antwort vom Server kommt
        //Wenn sich readystate verändert, dann wird immer Funktion ausgerufen
        //Adresse wird im Speicher abgelegt,
        //Erst Funktion angesprungen wenn sich Ready State ändert
        //ausgabe --> keine Klammern --> Funktion wird nur angegeben, soll nicht
        //aufgerufen werden was klammern machen
        xmlHttp.onreadystatechange = ausgabe;
        xmlHttp.send(null);
    }
    ausgabe();
    klicked();
}

//Button nach Klick ausgrauen
function klicked() {
    document.form.json_show_button.disabled=true;
}
// Rücklieferung von Webserver an Browser -- Ausgabe
function ausgabe() {
    //0 - nicht initialisiert, 1= lädt, 2 = fertig geladen, 3 - wartend, 4 - fertig
    if (xmlHttp.readyState == 4) {
        var daten = xmlHttp.responseText;
        // "Daten" (Objekte) werden umgewandelt
        daten = eval("(" + daten + ")");
        // eine Liste wird geschrieben
        var liste = document.getElementById("list");
        // Daten werden ausgegeben und nacheinander angehängt
        for (var i = 0; i < daten.length; i++) {
            var link = daten[i];
            // HTML-Elemente werden ebenfalls erstellt
            var li = document.createElement("li");
            var a = document.createElement("a");
            a.setAttribute("href", link.url);
            var txt = document.createTextNode(link.text);
            a.appendChild(txt);
            li.appendChild(a);
            liste.appendChild(li);
        }
    }
}





