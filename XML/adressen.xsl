<?xml version="1.0" encoding="ISO-8859-1" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:template match="/">
		<html>
			<head>
			</head>
			<body>
				<p align="center" style="font-family:Georgia; font-size:12pt; color:blue;">
		      <xsl:apply-templates />
				</p>
			</body>
		</html>
	</xsl:template>

	<xsl:template match="Name">
		<br />
		<br />
    <b>
			<xsl:value-of select="." />
    </b>
    <br />
	</xsl:template>

	<xsl:template match="Strasse">
			<xsl:value-of select="." />
	</xsl:template>

	<xsl:template match="Hausnummer">
  	<xsl:value-of select="." />
    <br />
	</xsl:template>

	<xsl:template match="PLZ">
		<font color="black">
  	  <xsl:value-of select="." />
  	</font>
	</xsl:template>

	<xsl:template match="Ort">
		<font color="black">
  	  <xsl:value-of select="." />
  	</font>
    <br />
    <br />
	</xsl:template>

	<xsl:template match="Bundesland">
		<i>
			<xsl:value-of select="." />
    </i>
    <br />
	</xsl:template>

</xsl:stylesheet>
