// es wird ein Array erstellt, mit allen Bildern der Show
var slideshow = new Array("./slideshow/slideshow1.jpg", "./slideshow/slideshow2.jpg", "./slideshow/slideshow3.jpg", "./slideshow/slideshow4.jpg");
// Array-Index des nachfolgenden Bildes wird festgelegt
var nextPicture = 0;
// Zeit bis zum n�chsten Bild, in Millisekunden (1 sek = 1000 millisek)
var zeit = 4000;

function load(){
	// startet die slideshow fuer das bild mit der id sshow
	animation('sshow');
}

function animation(id) {
	// prueft auf ein gueltiges element
	if(document.getElementById(id)){
		document.getElementById(id).src = slideshow[nextPicture];
		nextPicture++;
		// falls das Ende der Bilder erreicht ist...
		if(nextPicture == slideshow.length){
			// ... wird der Index wieder auf 0 gesetzt
			nextPicture = 0;
		}
		// nach dieser Zeit ruft Animation sich selbst noch einmal auf
		setTimeout("animation('"+id+"');", zeit);
	}
}