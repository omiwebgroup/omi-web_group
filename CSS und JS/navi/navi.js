var openedSubMenu = 0; //Startwert fuer ausgeklappte Rubrik
var Timer; //timer f�r die automatische menueschliessung

function closeSubMenu(id){
	if(isNaN(id)){
		// setzt alle ul unterelemente des submenus container unsichtbar
		document.getElementById('submenu').getElementsByTagName('ul').style.display = 'none';
	}else{
		// prueft ob es ein g�ltiges element ist
		if(document.getElementById('submenu_' + id)){
			// setzt das element unsichtbar
			document.getElementById('submenu_' + id).style.display = 'none';
		}
	}
}

function openSubMenu(id){
	// prueft ob eine gueltige nummer uebergeben wurde
	if(!isNaN(id)){
		// prueft ob das element gueltig ist
		if(document.getElementById('submenu_' + id)){
			// loescht den timer
			clearTimeout(Timer);
			// schliesst das aktuelle menue
			closeSubMenu(openedSubMenu);
			// setzt das gewaehlte submenu sichtbar
			document.getElementById('submenu_' + id).style.display = 'block';
			// speichert das aktuell geoeffnete submenu
			openedSubMenu = id;
		}
	}
}

function setTimer(id){
	// prueft ob eine gueltige nummer uebergeben wurde
	if(!isNaN(id)){
		// prueft ob das element gueltig ist
		if(document.getElementById('submenu_' + id)){
			// leoscht den Timer, da einige browser probleme haben und der alte timer bleibt bestehen
			clearTimeout(Timer);
			// setzt den timer neu
			Timer = setTimeout('closeSubMenu('+id+');', 1000);
		}
	}
}