// Kapitel 11.1.8 Tipp: Browserkompatibilit�ten
var xmlHttp;
if(window.ActiveXObject){
	try{
		// 1. Versuch: neue Internet Explorer (ab IE8)
		xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
	}catch(e){
		try{
			xmlHttp = new ActiveXObject(Microsoft.XMLHTTP);
		}catch(e){
			alert("Objekt nicht erfolgreich erstellt!");
		}
	}
} else if(window.XMLHttpRequest){
	xmlHttp = new XMLHttpRequest();
}

function ichSchickDirWasMitGET() {
	if(xmlHttp != null){
    document.getElementById("data").innerHTML="";
		xmlHttp.open("GET", "daten.php?1=London&2=Berlin&3=Paris", true);
		xmlHttp.onreadystatechange = ausgabe;
		xmlHttp.send(null);
	}
}

function ichSchickDirWasMitPOST() {
	if(xmlHttp != null){
    document.getElementById("data").innerHTML="";
		xmlHttp.open("POST", "daten.php", true);
		xmlHttp.onreadystatechange = ausgabe;
        //es wird gesagt in welchem Format gesendet wird
		xmlHttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded")
		//Zusatzinfos die gesendet werden
        xmlHttp.send("1=London&2=Berlin&3=Paris");
	}
}

function ausgabe(){
  if(xmlHttp.readyState == 4){
   	document.getElementById("data").insertAdjacentHTML("AfterBegin", xmlHttp.responseText);
  }
}
