// Kapitel 11.1.8 Tipp: Browserkompatibilitäten
var xmlHttp;
if(window.ActiveXObject){
	try{
		// 1. Versuch: neue Internet Explorer (ab IE8)
		xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
	}catch(e){
		try{
			xmlHttp = new ActiveXObject(Microsoft.XMLHTTP);
		}catch(e){
			alert("Objekt nicht erfolgreich erstellt!");
		}
	}
}else if(window.XMLHttpRequest){
	xmlHttp = new XMLHttpRequest();
}

function sagHallo() {
	if(xmlHttp != null){
		xmlHttp.open("GET", "hallo01.php", true);
		xmlHttp.onreadystatechange = ausgabe;
		xmlHttp.send(null);
	}
}

function ausgabe(){
  if(xmlHttp.readyState == 4){
   	document.getElementById("data").insertAdjacentHTML("AfterBegin", xmlHttp.responseText);
  }
}
