// Kapitel 11.1.8 Tipp: Browserkompatibilitäten
var xmlHttp;
if(window.ActiveXObject){
	try{
		// 1. Versuch: neue Internet Explorer (ab IE8)
		xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
	}catch(e){
		try{
			xmlHttp = new ActiveXObject(Microsoft.XMLHTTP);
		}catch(e){
			alert("Objekt nicht erfolgreich erstellt!");
		}
	}
}else if(window.XMLHttpRequest){
	xmlHttp = new XMLHttpRequest();
}

function sendeJSON_Array(){
	if(xmlHttp != null){
    var arrayJSON = [ document.getElementById("feld_1").value , document.getElementById("feld_2").value ];
    var stringJSON = JSON.stringify( arrayJSON );
    var stringBrowser = "1=" + stringJSON;
		xmlHttp.open("POST", "json03.php", true);
		xmlHttp.onreadystatechange = ausgabe;
		xmlHttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    xmlHttp.setRequestHeader("Content-Length",stringBrowser.length);
   	document.getElementById("data").insertAdjacentHTML(
   	  "BeforeEnd",
   	  "<hr />So wird es vom Browser zum Webserver gesendet:<br />" + stringBrowser + "<br />"
   	);
		xmlHttp.send(stringBrowser);
  }
}

function ausgabe(){
  if(xmlHttp.readyState == 4){
  	var stringJSON = xmlHttp.responseText;
   	document.getElementById("data").insertAdjacentHTML(
   	  "BeforeEnd",
   	  "<br />So empfängt es der Browser vom Webserver:<br />" + stringJSON + "<br />"
   	);
   	var arrayFelder =  eval(stringJSON);
    document.getElementById("feld_1").value = arrayFelder[0];
    document.getElementById("feld_2").value = arrayFelder[1];
  }
}
