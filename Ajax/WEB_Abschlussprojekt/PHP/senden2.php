<?php
/**
 * Created by IntelliJ IDEA.
 * User: Marc Enders
 * Date: 03.07.14
 * Time: 21:43
 */

$ip = $_SERVER["REMOTE_ADDR"];
$host = gethostbyaddr($ip);

$string = "Anbei Zusatzinfos\r\n" . "Vorname: " . $_POST["vorname"] . ", Nachname: " . $_POST["nachname"] .
    ", e-Mail: " . $_POST["mail"] . ", IP-Adresse: " . $ip . ", Hostname:" . $host;

$header = 'From: newsletter@marc.riwiki.de' . "\r\n" .
    'Reply-To: hostmaster@riwiki.de' . "\r\n" .
    'X-Mailer: PHP/' . phpversion();

$status = mail("1.8t.aeb@gmail.com", "News-Anmeldung", $string, $header, "-f hostmaster@riwiki.de");

?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>News</title>
    <link rel="stylesheet" type="text/css" href="../CSS/Style.css">
    <script src="JS/check_news_formular.js" type="text/javascript"/>
    <meta charset="utf-8"/>
</head>
    <body>
        <div id="outer_container">
            <div id="container">
                <div id="top"></div>
                <div id="logo"></div>
                <div>
                    <ul id="menue_nav">
                        <li><a href="../index.html">Startseite</a></li>
                        <li><a class="changecolor_after_click_green" href="../news.html">News</a></li>
                        <li><a href="../umbau.html">Umbau</a></li>
                        <li><a href="../links.html">Links</a></li>
                        <li><a href="../other.html">Other</a></li>
                        <li><a href="../mpressum.html">Impressum</a></li>
                    </ul>
                </div>
                <div id="inhalt_background_wallpaper">
                    <div id="greenbox_main">
                        <p class="h1_ueberschrift_left_15">
                            News: #JS #PHP
                        </p><br/>
                        <p class="h2_black_text_30_php">
                            Ihre Angaben:
                        </p>
                        <p class="h2_black_text_20_php">
                        <?php
                        echo "Ihr Nachname: " . $_POST["nachname"] . "<br>";
                        echo "Ihr Vorname: " . $_POST["vorname"] . "<br>";
                        echo "Ihr e-Mail: " . $_POST["mail"] . "<br>";
                        if($status) echo "<p class='h2_red_text_16_php'>Ihre Anmeldung war erfolgreich!</p>";
                        else echo "Leider ist ein Fehler aufgetreten";
                        ?>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>