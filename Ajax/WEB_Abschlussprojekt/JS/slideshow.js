/**
 * Created by Marc Enders on 03.07.14.
 */
/* Array mit Bildersammlung */
var bildershow = new Array("JS/img/1.1.png", "JS/img/1.2.png", "JS/img/1.5.png", "JS/img/1.6.png",
    "JS/img/1.7.png", "JS/img/1.8.png", "JS/img/1.9.png", "JS/img/1.10.png");

var nextpic = 0;
var time = 5000;

/* Funktion wenn der "auto" Button geklickt wurde */
function animation(){
    document.getElementById('bshow').src = bildershow[nextpic];
    nextpic++;

    if (nextpic == bildershow.length){
        nextpic = 0;
        animation()
    } else
    setTimeout("animation();", time);
}

/*Funktion wenn der der prev oder der next Button gedrückt wurde*/
function vor(wert){

    if (wert) {
        nextpic++;
    } else {
        nextpic--;
    }

    if (nextpic == bildershow.length){
        nextpic = 0;
    } else if (nextpic < 0) {
        nextpic = bildershow.length - 1
    }
    document.getElementById('bshow').src = bildershow[nextpic];
}


