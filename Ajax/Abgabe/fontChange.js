// Funktion cssChange um Font zu ändern

function cssChange() {
    // Variable CHANGE das Elelement vom Typ text zuweisen
    var change = document.getElementById('text');
    // Variable button das Elelement button zuweisen
    var button = document.getElementById('button');
    // Wenn Fontfamilie Serifen hat
    if (change.style.fontFamily == "serif") {
        // ändere zu Serifenlos
        change.style.fontFamily = "sans-serif";
        // ändere Knopfbeschriftung zu Schriftart ändern
        button.value = "Schriftart ändern"
    } else {
        // sonst ändere Fontfamilie zu mit Serifen
        change.style.fontFamily = "serif";
        // #ndere Knopfbeschriftung zu Schriftart zurücksetzten
        button.value = "Schriftart zurücksetzen";
    }
}
