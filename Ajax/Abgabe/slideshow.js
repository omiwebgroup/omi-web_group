// Array mit allen Bildern
var slideshow = new Array("./images/01.jpg", "./images/02.jpg", "./images/03.jpg",
"./images/04.jpg", "./images/05.jpg", "./images/06.jpg", "./images/07.jpg", "./images/08.jpg", "./images/09.jpg", "./images/10.jpg", "./images/11.jpg", "./images/12.jpg");
// Index des nächsten Bildes
var nextPicture = 0;
// Zeit bis zum nächsten Bild in ms
var zeit = 3000;

function load() {
	// Startet slideshow mit dem Bild mit der id sshow
	animation('sshow');
}

function animation(id) {
	// prüft auf gültiges Element
	if (document.getElementById(id)) {
		document.getElementById(id).src = slideshow[nextPicture]; //wenn Bild gleiche id hat...
		nextPicture++; //...ein Bild weiter

		// wenn Ende der Bilder erreicht...
		if (nextPicture == slideshow.length) {
			//...setze den index wieder auf 0 also fange wieder mit dem ersten Bild an
			nextPicture = 0;
		}
		// nach dieser Zeit ruft sich die Funktion animation erneut selber auf ..die slideshow entsteht..
		setTimeout("animation('"+id+"');", zeit);
	}
}