<?xml version="1.0" encoding="ISO-8859-1" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <html>
            <head>
                <style>
                    /*
                     <![CDATA[ */
                    ol {
                    counter-reset: Kapitel;
                    list-style-type: none;
                    }

                    li:before {
                        content: counters(Kapitel, ".") " ";
                        counter-increment: Kapitel;
                    }

                    li:first-child:before {
                        content: "0";
                        counter-reset: Kapitel;
                        counter-increment;
                    }

                    ol.Unterkapitel li:first-child:before {
                      content: counters(Kapitel, ".") " ";
                      counter-reset: none;
                      counter-increment: Kapitel;

                    /*]]> /*
                </style>
            </head>
            <body>
                <ul>
                    <xsl:apply-templates />
                </ul>
            </body>
        </html>
    </xsl:template>

    <!--



    <xsl:template match="UeberschriftLerneinheit">
        <br />
        <p align="center" style="font-family:Georgia; font-size:14pt;">
-->
    <!--
    <xsl:for-each select="UeberschriftLerneinheit">
        <xsl:value-of select="." />
    </xsl:for-each>
    <xsl:for-each select="UeberschriftKapitel">
    </xsl:for-each>
     -->
    <!--

                <xsl:number level="any" count="UeberschriftLerneinheit" format="1. " />

                <xsl:value-of select="." />
            </p>
        </xsl:template>



            <xsl:template match="UeberschriftKapitel">
                <font color="green">
                    <p align="center" style="font-family:Georgia; font-size:10pt;">
                        <xsl:number level="any" count="UeberschriftKapitel" format="1. " />
                        <xsl:value-of select="." />
                    </p>
                </font>
            </xsl:template>
             -->

    <xsl:template match="Web-Programmierung">
        <xsl:apply-templates select="Kapitel" />
    </xsl:template>

    <xsl:template match="Kapitel">
        <li>
        <xsl:value-of select="Ueberschrift" />
        <xsl:if test="child::Unterkapitel">
            <ul class="Unterkapitel">
                <xsl:apply-templates select="child::Unterkapitel" />
            </ul>
        </xsl:if>
        </li>
    </xsl:template>
</xsl:stylesheet>
