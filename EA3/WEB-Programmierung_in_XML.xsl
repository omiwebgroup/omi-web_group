<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <html>
      <head>
        <title>Kapitelübersicht</title>
      </head>
      <body>
        <xsl:apply-templates />
        <br />
      </body>
    </html>
  </xsl:template>
  <xsl:template match="Web-Programmierung">
    <xsl:for-each select="Web-Programmierung">
      <xsl:for-each select="Hauptteil">
        <h1>
          <xsl:value-of select="." />
        </h1>
      </xsl:for-each>
      <xsl:for-each select="Kapitel">
        <h2>
          <xsl:value-of select="." />
        </h2>
        <xsl:for-each select="Unterkapitel">
          <h3>
            <xsl:value-of select="." />
          </h3>
        </xsl:for-each>
      </xsl:for-each>
    </xsl:for-each>
  </xsl:template>
</xsl:stylesheet>
