/* <![CDATA[ */
var xmlHttp = null;
if (window.XMLHttpRequest) {
    xmlHttp = new XMLHttpRequest();
} else if (window.ActiveXObject) {
    xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
}
if (xmlHttp != null) {
    xmlHttp.open("GET", "./json_link.txt", true);
    xmlHttp.onreadystatechange = ausgabe;
    xmlHttp.send(null);
}

function ausgabe() {
    if (xmlHttp.readyState == 4) {
        var daten = xmlHttp.responseText;
        // "Daten" (Objekte) werden umgewandelt
        daten = eval("(" + daten + ")");
        // eine Liste wird geschrieben
        var liste = document.getElementById("listing");
        // Daten werden ausgegeben und nacheinander angeh�ngt
        for (var i = 0; i < daten.length; i++) {
            var link = daten[i];
            // HTML-Elemente werden ebenfalls erstellt
            var li = document.createElement("li");
            var a = document.createElement("a");
            a.setAttribute("href", link.url);
            var txt = document.createTextNode(link.text);
            a.appendChild(txt);
            li.appendChild(a);
            liste.appendChild(li);
        }
    }
}
/* ]]> */
